#!/bin/bash

source bin/activate
xargs kill <pid
rm pid
echo "Stopped server..."
deactivate
