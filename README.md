# REST API for database query
This REST API lets you query the RESE2NSE local database containing all the data from the networks that have been set up. Entering various URLs in your browser acts as database queries, output format is in JSON.

# Different APIs
There are different APIs for different needs. The REST server can be accessed via the IP address `10.158.9.12:6969`. The API of choice then follows.

## **Aggregator/Sniffer list**
Each network has its own Aggregator and Sniffer. To access the data sent by a specific aggregator or sniffer, you need its ID. To list the aggregator or sniffer IDs that are available, enter the following URLs:


[`10.158.9.12:6969/aggregator`](http://10.158.9.12:6969/aggregator) to bring up the Aggregator ID list, and 
[`10.158.9.12:6969/v1sniffer`](http://10.158.9.12:6969/v1sniffer) to bring up the Sniffer ID list.

## **Aggregator-based APIs**
When you now know the specific aggregator data that you want to access, there are a couple of APIs that you may use, depending on if you want to retrieve the list of nodes that are sending data to the aggregator, or if you want to retrieve the node data themselves.

### Node list
[`10.158.9.12:6969/aggregator/<id>/nodelist`](http://10.158.9.12:6969/aggregator/2/nodelist) brings out the list of nodes that have successfully sent data to the aggregator for the past 30 minutes. If you want to list the active nodes for a specific timeframe instead, you may use these APIs:

### Last known transmission from each node
[`10.158.9.12:6969/aggregator/<id>/lasttransmit`](http://10.158.9.12:6969/aggregator/2/lasttransmit) shows the list of the last known transmitted packets of each node in the network.

#### Recent
[`10.158.9.12:6969/aggregator/<id>/nodelist/recent/<duration>`](http://10.158.9.12:6969/aggregator/2/nodelist/recent/10m) shows the active nodes for the past specified duration. The duration has a format similar to "10m" which translates to 10 minutes. Different units such as hours (`12h`: 12 hours) or even days (`2d`: 2 days) may be used in this API.

#### Start and End Time
[`10.158.9.12:6969/aggregator/<id>/nodelist/start/<starttime>`](http://10.158.9.12:6969/aggregator/2/nodelist/start/2021-12-28+0:00:00)

[`10.158.9.12:6969/aggregator/<id>/nodelist/start/<starttime>/end/<endtime>`](http://10.158.9.12:6969/aggregator/2/nodelist/start/2021-12-28+0:00:00/end/2021-12-29+0:00:00)

You may use these APIs to specify the timeframe that you want to look into. The format for starttime and endtime is `YYYY-MM-DD+H:MM:SS`, so querying between Nov 25, 2021-midnight and Nov 27, 1:00:00 pm should result in starttime = `2021-12-25+0:00:00` and endtime = `2021-12-27+13:00:00`. Not specifying endtime (hence, using the former API format) sets the endtime to the current time.

### Data
There are two APIs that can be used for accessing the node data: raw, and sensor. Using the raw API returns the raw packets sent by the nodes, and the sensor API returns a parsed version of these raw packets. NOTE: packet parsing is done on demand since our database only currently stores the raw packets. Using /sensor APIs may take slightly longer than /raw APIs.

**For all data queries (both aggregator and sniffer), the REST server's output is `[length,[data]]`**

[`10.158.9.12:6969/aggregator/<id>/raw/recent/<duration>`](http://10.158.9.12:6969/aggregator/2/raw/recent/10m)

[`10.158.9.12:6969/aggregator/<id>/sensor/recent/<duration>`](http://10.158.9.12:6969/aggregator/2/sensor/recent/10m)

[`10.158.9.12:6969/aggregator/<id>/raw/start/<starttime>`](http://10.158.9.12:6969/aggregator/2/raw/start/2021-12-28+0:00:00)

[`10.158.9.12:6969/aggregator/<id>/raw/start/<starttime>/end/<endtime>`](http://10.158.9.12:6969/aggregator/2/raw/start/2021-12-28+0:00:00/end/2021-12-29+0:00:00)

[`10.158.9.12:6969/aggregator/<id>/sensor/start/<starttime>`](http://10.158.9.12:6969/aggregator/2/sensor/start/2021-12-28+0:00:00)

[`10.158.9.12:6969/aggregator/<id>/sensor/start/<starttime>/end/<endtime>`](http://10.158.9.12:6969/aggregator/2/sensor/start/2021-12-28+0:00:00/end/2021-12-29+0:00:00)

Same rules for duration, starttime, and endtime apply.

#### Node-specific Queries
You may want to retrieve the data sent by a specific node (again, the node list for a certain aggregator can be accessed via [`10.158.9.12:6969/aggregator/<id>/nodelist`](http://10.158.9.12:6969/aggregator/2/nodelist)). To do so, you may insert a node ID parameter after the aggregator id:

[`10.158.9.12:6969/aggregator/<id>/node/<nodeID>/raw/recent/<duration>`](http://10.158.9.12:6969/aggregator/2/node/0013A2004146EF32/raw/recent/10m)

[`10.158.9.12:6969/aggregator/<id>/node/<nodeID>/sensor/recent/<duration>`](http://10.158.9.12:6969/aggregator/2/node/0013A2004146EF32/raw/recent/10m)

[`10.158.9.12:6969/aggregator/<id>/node/<nodeID>/raw/start/<starttime>`](http://10.158.9.12:6969/aggregator/2/node/0013A2004146EF32/raw/start/2021-12-28+0:00:00)

[`10.158.9.12:6969/aggregator/<id>/node/<nodeID>/raw/start/<starttime>/end/<endtime>`](http://10.158.9.12:6969/aggregator/2/node/0013A2004146EF32/raw/start/2021-12-28+0:00:00/end/2021-12-29+0:00:00)

[`10.158.9.12:6969/aggregator/<id>/node/<nodeID>/sensor/start/<starttime>`](http://10.158.9.12:6969/aggregator/2/node/0013A2004146EF32/sensor/start/2021-12-28+0:00:00)

[`10.158.9.12:6969/aggregator/<id>/node/<nodeID>/sensor/start/<starttime>/end/<endtime>`](http://10.158.9.12:6969/aggregator/2/node/0013A2004146EF32/sensor/start/2021-12-28+0:00:00/end/2021-12-29+0:00:00)

Same rules for duration, starttime, and endtime apply. As per usual, you may specify if you want to retrieve the raw packets or its parsed version.

## **Sniffer-based APIs**
Accessing sniffer data is very similar to aggregator data. You will just need to replace the 'aggregator' field into 'v1sniffer', and the same format will follow. As of now, you may only retrieve the raw packets detected by the sniffer.

[`10.158.9.12:6969/v1sniffer/<id>/raw/recent/<duration>`](http://10.158.9.12:6969/v1sniffer/2/raw/recent/10m)

[`10.158.9.12:6969/v1sniffer/<id>/raw/start/<starttime>`](http://10.158.9.12:6969/v1sniffer/2/raw/start/2021-12-28+0:00:00)

[`10.158.9.12:6969/v1sniffer/<id>/raw/start/<starttime>/end/<endtime>`](http://10.158.9.12:6969/v1sniffer/2/raw/start/2021-12-28+0:00:00/end/2021-12-29+0:00:00)

### Packet Categorization & Counting (moving sum size is set to 5 minutes by default)
Even if the parsed version of the raw packets are currently unavailable, you may query for a count of the different packet categories described in the stats section of the [data-analysis-jupyter-notebooks repository](https://gitlab.eee.upd.edu.ph/rese2nse/data-analysis-jupyter-notebooks) README. NOTE: again, since the database only currently stores raw packets on-demand parsing is performed for these APIs, and thus, may take longer than raw packet queries. For quicker quieries, you may use the [`pstats`](#pstats) APIs instead for automatically post-processed stats with predefined intervals.

The APIs are as follows:

[`10.158.9.12:6969/v1sniffer/<id>/stats/recent/<duration>`](http://10.158.9.12:6969/v1sniffer/2/stats/recent/10m)

[`10.158.9.12:6969/v1sniffer/<id>/stats/start/<starttime>`](http://10.158.9.12:6969/v1sniffer/2/stats/start/2021-12-28+0:00:00)

[`10.158.9.12:6969/v1sniffer/<id>/stats/start/<starttime>/end/<endtime>`](http://10.158.9.12:6969/v1sniffer/2/stats/start/2021-12-28+0:00:00/end/2021-12-29+0:00:00)


#### Specifying the moving sum window size
The packet counts that you will see are actually from moving sums. If not specified, the window size of the moving sum is set to `5m` (5 minutes) by default. If you want to specify your own window size, simply add a winsize parameter to the end of the previously shown APIs:

[`10.158.9.12:6969/v1sniffer/<id>/stats/recent/<duration>/winsize/<winsize>`](http://10.158.9.12:6969/v1sniffer/2/stats/recent/10m/winsize/3m)

[`10.158.9.12:6969/v1sniffer/<id>/stats/start/<starttime>/winsize/<winsize>`](http://10.158.9.12:6969/v1sniffer/2/stats/start/2021-12-28+0:00:00/winsize/1h)

[`10.158.9.12:6969/v1sniffer/<id>/stats/start/<starttime>/end/<endtime>/winsize/<winsize>`](http://10.158.9.12:6969/v1sniffer/2/stats/start/2021-12-28+0:00:00/end/2021-12-29+0:00:00/winsize/1h)

#### Node-specific Packet Counts
Similar to the node-specific queries for the aggregator, you may add a node ID field after the sniffer ID to get the stats for the packets that a specific node is sending.

[`10.158.9.12:6969/v1sniffer/<id>/node/<nodeID>/stats/recent/<duration>`](http://10.158.9.12:6969/v1sniffer/2/node/0013A2004146EF32/stats/recent/10m)

[`10.158.9.12:6969/v1sniffer/<id>/node/<nodeID>/stats/recent/<duration>/winsize/<winsize>`](http://10.158.9.12:6969/v1sniffer/2/node/0013A2004146EF32/stats/recent/10m/winsize/3m)

Unfortunately, the starttime/endtime APIs still haven't been implemented for the node-specific packet counts.

### PSTATS
Similar to the `stats` APIs, but the stats are automatically processed in predefined intervals and stored into the databases to avoid the slower on-demand processing queries. When not specified, the API uses a `30m` window size by default.

[`10.158.9.12:6969/v1sniffer/<id>/pstats/recent/<duration>`](http://10.158.9.12:6969/v1sniffer/2/pstats/recent/5h)

Similar to `stats`, you can specify the window size. Currently, there are six available window sizes:`30m`, `1h`, `6h`, `12h`, `1d`, and `1m10m`. Using `winsize=30m` means that each entry is 30 minutes apart, and the window sizes (for the rolling window counts) are also 30 minutes long. Same goes with `1h`, `6h`, `12h`, and `1d`. `winsize=1m10m` is a special case for visualization app purposes: each entry is separated by 1 minute, but the rolling window size is 10 minutes.

[`10.158.9.12:6969/v1sniffer/<id>/pstats/recent/<duration>/winsize/<winsize>`](http://10.158.9.12:6969/v1sniffer/2/pstats/recent/5h/winsize/1h)
