from flask import Flask,jsonify,request
import mysql.connector as database
import json
import re
import pandas as pd
from datetime import datetime
from datetime import timedelta
import os
import logging
from rese2nse_nwk_utils import decoder as d
from rese2nse_nwk_utils import packet_count as p
from config import *

#logging.basicConfig(filename='demo.log', level=logging.DEBUG)

app = Flask(__name__)

def conv_unit(unit):
    if(unit == 'd' or unit == 'D'):
        return 'DAY'
    elif(unit == 'h' or unit == 'H'):
        return 'HOUR'
    elif(unit == 'm' or unit == 'M'):
        return 'MINUTE'
    else:
        return
    
def check_row_format(table):
    idx_correct_format = (table[1].str[4:6] == '00') & (table[1].str[10:12] == '01') & (table[1].str[16:18] == '04') & (table[1].str[22:24] == '05') & (table[1].str[28:30] == '07')
    return idx_correct_format
    
def decode_agg_packet(table):
    idx = check_row_format(table)
    #convert to str ulit para consistent formatting lol
    
    #parse all valid rows
    table.loc[idx,'0'] = table.loc[idx,1].str[6:10].apply(int,base=16).apply(str)
    table.loc[idx,'1'] = table.loc[idx,1].str[12:16].apply(int,base=16).apply(str)
    table.loc[idx,'4'] = table.loc[idx,1].str[18:22].apply(int,base=16).apply(str)
    table.loc[idx,'5'] = table.loc[idx,1].str[24:28].apply(int,base=16).apply(str)
    nodeinfo = table.loc[idx,1].str[30:]
    nodeinfo = nodeinfo.str.split('3a',expand=True)
    table.loc[idx,'nodeName'] = nodeinfo[0].apply(lambda x: bytes.fromhex(x).decode('utf-8'))
    table.loc[idx,'nodeLoc'] = nodeinfo[1].apply(lambda x: bytes.fromhex(x).decode('utf-8'))
    
    #parse all data fields of invalid rows to 'None'
    table.loc[~idx,'0'] = None
    table.loc[~idx,'1'] = None
    table.loc[~idx,'4'] = None
    table.loc[~idx,'5'] = None
    table.loc[~idx,'nodeName'] = None
    table.loc[~idx,'nodeLoc'] = None    
    
    table = table.drop(1,axis=1)
    table.rename(columns={0:'ts',2:'nodeID'},inplace=True)

    return table

@app.route('/', methods=['GET'])
def doc():
    if request.method == 'GET':
        return jsonify('This is the 1 Feb 2022, 4:40pm version. Changed column names in query to reflect changes in DB.')
    
########################
#    auxiliary APIs    #
########################

@app.route('/showdb', methods=['GET'])
def showdb():
    conn = database.connect(user = user,
                           password = password,
                           host = host)
    
    cur = conn.cursor()
    cur.execute("SHOW DATABASES")
    rv = cur.fetchall()
    
    return jsonify(rv)

@app.route('/aggregator', methods=['GET'])
def show_aggregators():
    conn = database.connect(user = user,
                           password = password,
                           host = host)
    
    cur = conn.cursor()
    cur.execute("SHOW DATABASES LIKE 'aggre_data_raw_%'")
    rv = cur.fetchall()
    
    result = [item[0].replace('aggre_data_raw_','') for item in rv]
    
    return jsonify(result)

@app.route('/aggregator/<id>/showtables', methods=['GET'])
def showtables(id):
    dbname = 'aggre_data_raw_' + str(id)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    cur = conn.cursor()
    cur.execute('SHOW TABLES')
    rv = cur.fetchall()
    
    return jsonify(rv)

@app.route('/aggregator/<id>/showcols', methods=['GET'])
def showcols(id):
    dbname = 'aggre_data_raw_' + str(id)
    table = 'aggre_packet'
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    cur = conn.cursor()
    sqlText = f"SHOW COLUMNS from {table}"
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    return jsonify(rv)

########################
#    node list APIs    #
########################

#nodelist recent
@app.route('/aggregator/<id>/nodelist', methods = ['GET'])
@app.route('/aggregator/<id>/nodelist/recent/<dur>', methods = ['GET'])
def get_nodelist2(id,dur='30m'):
    dbname = 'aggre_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    
    unit = conv_unit(unit)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT DISTINCT nodeID FROM aggre_packet WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    rv = [x[0] for x in rv]
    
    return jsonify(rv)

#nodelist start end
@app.route('/aggregator/<id>/nodelist/start/<start>', methods = ['GET'])
@app.route('/aggregator/<id>/nodelist/start/<start>/end/<end>', methods = ['GET'])
def get_nodelist4(id,start,end=-1):
    dbname = 'aggre_data_raw_' + str(id)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    if(end == -1):
        sqlText = f"SELECT DISTINCT nodeID FROM aggre_packet WHERE (ts BETWEEN '{start}' AND NOW()) ORDER BY ts ASC"
    else:
        sqlText = f"SELECT DISTINCT nodeID FROM aggre_packet WHERE (ts BETWEEN '{start}' AND '{end}') ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    rv = [x[0] for x in rv]
    
    return jsonify(rv)

########################
#    node list APIs    #
#  LAST KNOWN TRANSMIT #
########################

#nodelist recent
@app.route('/aggregator/<id>/lasttransmit', methods = ['GET'])
def get_lastknowntransmit(id):
    dbname = 'aggre_data_parsed_' + str(id)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT * FROM aggre_packet WHERE id IN (SELECT MAX(id) FROM aggre_packet GROUP BY nodeID)"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    headers = [x[0] for x in cur.description]
    result = [dict(zip(headers,row)) for row in rv]
    
    return jsonify(result)

########################
#    node-specific     #
#          APIs        #
########################

#sensor all recent
@app.route('/aggregator/<id>/node/<node>/sensor/recent/<dur>', methods = ['GET'])
def get_nodeall1(id,node,dur):
    dbname = 'aggre_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)
    
    node_mac = node.upper()
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT ts,packet FROM aggre_packet WHERE ts >= NOW() - INTERVAL {num} {unit} AND nodeID = '{node_mac}' ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        table = pd.DataFrame.from_dict(rv)
        table = decode_agg_packet(table)
        #table.to_json(orient='records')

        return jsonify(len(rv),table.to_dict(orient='records'))
    else:
        return jsonify(0,[])
    
#sensor all start end
@app.route('/aggregator/<id>/node/<node>/sensor/start/<start>', methods = ['GET'])
@app.route('/aggregator/<id>/node/<node>/sensor/start/<start>/end/<end>', methods = ['GET'])
def get_nodeall3(id,node,start,end=-1):
    dbname = 'aggre_data_raw_' + str(id)
    
    node_mac = node.upper()
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    if(end == -1):
        sqlText = f"SELECT ts,packet FROM aggre_packet WHERE (ts BETWEEN '{start}' AND NOW()) AND nodeID = '{node_mac}' ORDER BY ts ASC"
    else:
        sqlText = f"SELECT ts,packet FROM aggre_packet WHERE (ts BETWEEN '{start}' AND '{end}') AND nodeID = '{node_mac}' ORDER BY ts ASC"
    
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        table = pd.DataFrame.from_dict(rv)
        table = decode_agg_packet(table)
        #table.to_json(orient='records')

        return jsonify(len(rv),table.to_dict(orient='records'))
    else:
        return jsonify(0,[])

#sensor sid recent
@app.route('/aggregator/<id>/node/<node>/sensor/<sid>/recent/<dur>', methods = ['GET'])
def get_sense1(id,node,sid,dur):
    dbname = 'aggre_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)
    
    node_mac = node.upper()
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT ts,packet FROM aggre_packet WHERE ts >= NOW() - INTERVAL {num} {unit} AND nodeID = '{node_mac}' ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        table = pd.DataFrame.from_dict(rv)
        table = decode_agg_packet(table)
        #table.to_json(orient='records')
        new_table = table.loc[idx,['ts',str(sid)]]

        return jsonify(len(rv),new_table.to_dict(orient='records'))
    else:
        return jsonify(0,[])

#sensor sid start end
@app.route('/aggregator/<id>/node/<node>/sensor/<sid>/start/<start>', methods = ['GET'])
@app.route('/aggregator/<id>/node/<node>/sensor/<sid>/start/<start>/end/<end>', methods = ['GET'])
def get_sense3(id,node,sid,start,end=-1):
    dbname = 'aggre_data_raw_' + str(id)
    node_mac = node.upper()
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    if(end == -1):
        sqlText = f"SELECT ts,packet FROM aggre_packet WHERE (ts BETWEEN '{start}' AND NOW()) AND nodeID = '{node_mac}' ORDER BY ts ASC"
    else:
        sqlText = f"SELECT ts,packet FROM aggre_packet WHERE (ts BETWEEN '{start}' AND '{end}') AND nodeID = '{node_mac}' ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        table = pd.DataFrame.from_dict(rv)
        table = decode_agg_packet(table)
        #table.to_json(orient='records')
        new_table = table.loc[idx,['ts',str(sid)]]

        return jsonify(len(rv),new_table.to_dict(orient='records'))
    else:
        return jsonify(0,[])

#raw recent
@app.route('/aggregator/<id>/node/<node>/raw/recent/<dur>', methods = ['GET'])
def get_rawdata1(id,node,dur):
    dbname = 'aggre_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)
    
    node_mac = node.upper()
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT ts,packet FROM aggre_packet WHERE ts >= NOW() - INTERVAL {num} {unit} AND nodeID = '{node_mac}' ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]

        result = [dict(zip(headers,row)) for row in rv]

        return jsonify(len(rv),result)
    else:
        return jsonify(0,[])

#raw start end
@app.route('/aggregator/<id>/node/<node>/raw/start/<start>', methods = ['GET'])
@app.route('/aggregator/<id>/node/<node>/raw/start/<start>/end/<end>', methods = ['GET'])
def get_rawdata2(id,node,start,end=-1):
    dbname = 'aggre_data_raw_' + str(id)
    
    node_mac = node.upper()
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    if(end == -1):
        sqlText = f"SELECT ts,packet FROM aggre_packet WHERE (ts BETWEEN '{start}' AND NOW()) AND nodeID = '{node_mac}' ORDER BY ts ASC"
    else:
        sqlText = f"SELECT ts,packet FROM aggre_packet WHERE (ts BETWEEN '{start}' AND '{end}') AND nodeID = '{node_mac}' ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]

        result = [dict(zip(headers,row)) for row in rv]

        return jsonify(len(rv),result)
    else:
        return jsonify(0,[])
    
########################
#   whole aggregator   #
#          APIs        #
########################

#raw recent
@app.route('/aggregator/<id>/raw/recent/<dur>', methods = ['GET'])
def get_allrawdata(id,dur):
    dbname = 'aggre_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT * FROM aggre_packet WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    headers = [x[0] for x in cur.description]

    result = [dict(zip(headers,row)) for row in rv]

    return jsonify(len(rv),result)

#raw start end
@app.route('/aggregator/<id>/raw/start/<start>', methods = ['GET'])
@app.route('/aggregator/<id>/raw/start/<start>/end/<end>', methods = ['GET'])
def get_rawdata_start_end(id,start,end=-1):
    dbname = 'aggre_data_raw_' + str(id)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    #start = start.replace('+',' ')
    if(end == -1):
        sqlText = f"SELECT * FROM aggre_packet WHERE (ts BETWEEN '{start}' AND NOW()) ORDER BY ts ASC"
    else:
        sqlText = f"SELECT * FROM aggre_packet WHERE (ts BETWEEN '{start}' AND '{end}') ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    headers = [x[0] for x in cur.description]

    result = [dict(zip(headers,row)) for row in rv]

    return jsonify(len(rv),result)

#parsed recent
@app.route('/aggregator/<id>/sensor/recent/<dur>', methods = ['GET'])
def get_allparsed1(id,dur):
    dbname = 'aggre_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT ts,packet,nodeID FROM aggre_packet WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        table = pd.DataFrame.from_dict(rv)
        table = decode_agg_packet(table)
        #table.to_json(orient='records')

        return jsonify(len(rv),table.to_dict(orient='records'))
    else:
        return jsonify(0,[])

#parsed start end
@app.route('/aggregator/<id>/sensor/start/<start>', methods = ['GET'])
@app.route('/aggregator/<id>/sensor/start/<start>/end/<end>', methods = ['GET'])
def get_allparsed2(id,start,end=-1):
    dbname = 'aggre_data_raw_' + str(id)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    if(end == -1):
        sqlText = f"SELECT ts,packet,nodeID FROM aggre_packet WHERE (ts BETWEEN '{start}' AND NOW()) ORDER BY ts ASC"
    else:
        sqlText = f"SELECT ts,packet,nodeID FROM aggre_packet WHERE (ts BETWEEN '{start}' AND '{end}') ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        table = pd.DataFrame.from_dict(rv)
        table = decode_agg_packet(table)
        #table.to_json(orient='records')

        return jsonify(len(rv),table.to_dict(orient='records'))
    else:
        return jsonify(0,[])
    
###############################
#        SNIFFER STUFF        #
###############################
@app.route('/v1sniffer', methods=['GET'])
def show_v1sniffers():
    conn = database.connect(user = user,
                           password = password,
                           host = host)
    
    cur = conn.cursor()
    cur.execute("SHOW DATABASES LIKE 'sniffer_data_raw_%'")
    rv = cur.fetchall()
    
    result = [item[0].replace('sniffer_data_raw_','') for item in rv]
    
    return jsonify(result)

@app.route('/v1sniffer/<id>/raw/recent/<dur>', methods = ['GET'])
def get_snifferrecent(id,dur):
    dbname = 'sniffer_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)    
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT * from sniffer_data WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    headers = [x[0] for x in cur.description]
    result = [dict(zip(headers,row)) for row in rv]
    
    return jsonify(len(rv),result)

@app.route('/v1sniffer/<id>/raw/start/<start>', methods = ['GET'])
@app.route('/v1sniffer/<id>/raw/start/<start>/end/<end>', methods = ['GET'])
def get_snifferstartend(id,start,end=-1):
    dbname = 'sniffer_data_raw_' + str(id)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    if(end == -1):
        sqlText = f"SELECT * FROM sniffer_data WHERE (ts BETWEEN '{start}' AND NOW()) ORDER BY ts ASC"
    else:
        sqlText = f"SELECT * FROM sniffer_data WHERE (ts BETWEEN '{start}' AND '{end}') ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    headers = [x[0] for x in cur.description]
    result = [dict(zip(headers,row)) for row in rv]
    
    return jsonify(len(rv),result)

########################
#    Sniffer packet    #
#      count APIs      #
########################
@app.route('/v1sniffer/<id>/stats/recent/<dur>', methods = ['GET'])
@app.route('/v1sniffer/<id>/stats/recent/<dur>/winsize/<winsize>', methods = ['GET'])
def get_snifferstats(id,dur,winsize='5m'):
    dbname = 'sniffer_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)

    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT * from sniffer_data WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        table = pd.DataFrame(rv,columns=headers)

        nwk = d.v1_nwk(0,0,0)
        nwk.raw_data = table
        nwk.raw_data = nwk.raw_data.applymap(str)
        nwk.v1decode()

        nwk.stats = p.nwk_stats(nwk.parsed_data_valid)
        nwk.stats.count_packets_network(nwk.parsed_data_valid,winsize)
        nwk.stats.count_nwk = nwk.stats.count_nwk.applymap(str)

        return jsonify(len(rv),nwk.stats.count_nwk.to_dict(orient='records'))
    else:
        return jsonify(0,[])

@app.route('/v1sniffer/<id>/stats/start/<start>', methods = ['GET'])
@app.route('/v1sniffer/<id>/stats/start/<start>/end/<end>', methods = ['GET'])
@app.route('/v1sniffer/<id>/stats/start/<start>/winsize/<winsize>', methods = ['GET'])
@app.route('/v1sniffer/<id>/stats/start/<start>/end/<end>/winsize/<winsize>', methods = ['GET'])
def get_snifferstatsstartend(id,start,end=-1,winsize='5m'):
    dbname = 'sniffer_data_raw_' + str(id)

    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    if(end == -1):
        sqlText = f"SELECT * FROM sniffer_data WHERE (ts BETWEEN '{start}' AND NOW()) ORDER BY ts ASC"
    else:
        sqlText = f"SELECT * FROM sniffer_data WHERE (ts BETWEEN '{start}' AND '{end}') ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        table = pd.DataFrame(rv,columns=headers)

        nwk = d.v1_nwk(0,0,0)
        nwk.raw_data = table
        nwk.raw_data = nwk.raw_data.applymap(str)
        nwk.v1decode()

        nwk.stats = p.nwk_stats(nwk.parsed_data_valid)
        nwk.stats.count_packets_network(nwk.parsed_data_valid,winsize)
        nwk.stats.count_nwk = nwk.stats.count_nwk.applymap(str)

        return jsonify(len(rv),nwk.stats.count_nwk.to_dict(orient='records'))
    else:
        return jsonify(0,[])

@app.route('/v1sniffer/<id>/node/<node>/stats/recent/<dur>', methods = ['GET'])
@app.route('/v1sniffer/<id>/node/<node>/stats/recent/<dur>/winsize/<winsize>', methods = ['GET'])
def get_snifferstatspernode(id,dur,node,winsize='5m'):
    dbname_snf = 'sniffer_data_raw_' + str(id)
    dbname_agg = 'aggre_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)

    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname_snf)
    
    conn_agg = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname_agg)
    
    sqlText = f"SELECT * from sniffer_data WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        table = pd.DataFrame(rv,columns=headers)

        nwk = d.v1_nwk(0,0,0)
        nwk.raw_data = table
        nwk.raw_data = nwk.raw_data.applymap(str)
        nwk.v1decode()

        #get node list
        cur = conn_agg.cursor()
        sqlText = f"SELECT DISTINCT nodeID FROM aggre_packet WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
        cur.execute(sqlText)
        nlist = cur.fetchall()
        nlist = [x[0].lower() for x in nlist]

        node = node.lower()

        nwk.stats = p.nwk_stats(nwk.parsed_data_valid)
        nwk.stats.count_packets_per_node(nwk.parsed_data_valid,nlist,winsize)
        nwk.stats.count[node] = nwk.stats.count[node].applymap(str)

        return jsonify(len(rv),nwk.stats.count[node].to_dict(orient='records'))
    else:
        return jsonify(0,[])

########################
#    Sniffer packet    #
#      count APIs      #
#      lite version    #
########################
@app.route('/v1sniffer/<id>/statslite/recent/<dur>', methods = ['GET'])
@app.route('/v1sniffer/<id>/statslite/recent/<dur>/winsize/<winsize>', methods = ['GET'])
def get_snifferstatslite(id,dur,winsize='5m'):
    dbname = 'sniffer_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)

    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT * from sniffer_data WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        table = pd.DataFrame(rv,columns=headers)

        nwk = d.v1_nwk(0,0,0)
        nwk.raw_data = table
        nwk.raw_data = nwk.raw_data.applymap(str)
        nwk.v1decode()

        nwk.stats = p.nwk_stats(nwk.parsed_data_valid)
        nwk.stats.count_packets_network(nwk.parsed_data_valid,winsize)
        nwk.stats.count_nwk = nwk.stats.count_nwk.applymap(str)

        return jsonify(len(rv),nwk.stats.count_nwk.values.tolist())
    else:
        return jsonify(0,[])


########################
#    Sniffer packet    #
#      count APIs      #
#      lite version    #
########################
@app.route('/v1sniffer/<id>/statslite/start/<start>', methods = ['GET'])
@app.route('/v1sniffer/<id>/statslite/start/<start>/end/<end>', methods = ['GET'])
@app.route('/v1sniffer/<id>/statslite/start/<start>/winsize/<winsize>', methods = ['GET'])
@app.route('/v1sniffer/<id>/statslite/start/<start>/end/<end>/winsize/<winsize>', methods = ['GET'])
def get_snifferstatslitestartend(id,start,end=-1,winsize='5m'):
    dbname = 'sniffer_data_raw_' + str(id)

    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    if(end == -1):
        sqlText = f"SELECT * FROM sniffer_data WHERE (ts BETWEEN '{start}' AND NOW()) ORDER BY ts ASC"
    else:
        sqlText = f"SELECT * FROM sniffer_data WHERE (ts BETWEEN '{start}' AND '{end}') ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        table = pd.DataFrame(rv,columns=headers)

        nwk = d.v1_nwk(0,0,0)
        nwk.raw_data = table
        nwk.raw_data = nwk.raw_data.applymap(str)
        nwk.v1decode()

        nwk.stats = p.nwk_stats(nwk.parsed_data_valid)
        nwk.stats.count_packets_network(nwk.parsed_data_valid,winsize)
        nwk.stats.count_nwk = nwk.stats.count_nwk.applymap(str)

        return jsonify(len(rv),nwk.stats.count_nwk.values.tolist())
    else:
        return jsonify(0,[])

########################
#    Sniffer packet    #
#      count APIs      #
#     AVERAGE BW       #
########################
@app.route('/v1sniffer/<id>/bw/recent/<dur>', methods = ['GET'])
@app.route('/v1sniffer/<id>/bw/recent/<dur>/winsize/<winsize>', methods = ['GET'])
def get_nwkbw(id,dur,winsize='5m'):
    dbname = 'sniffer_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)

    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT * from sniffer_data WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        table = pd.DataFrame(rv,columns=headers)

        nwk = d.v1_nwk(0,0,0)
        nwk.raw_data = table
        nwk.raw_data = nwk.raw_data.applymap(str)
        nwk.v1decode()

        nwk.stats = p.nwk_stats(nwk.parsed_data)
        nwk.stats.get_ave_bw(nwk.parsed_data,winsize)

        return jsonify(len(rv),nwk.stats.ave_bw.to_dict(orient='records'))
    else:
        return jsonify(0,[])

@app.route('/v1sniffer/<id>/bw/start/<start>', methods = ['GET'])
@app.route('/v1sniffer/<id>/bw/start/<start>/end/<end>', methods = ['GET'])
@app.route('/v1sniffer/<id>/bw/start/<start>/winsize/<winsize>', methods = ['GET'])
@app.route('/v1sniffer/<id>/bw/start/<start>/end/<end>/winsize/<winsize>', methods = ['GET'])
def get_nwkbwstartend(id,start,end=-1,winsize='5m'):
    dbname = 'sniffer_data_raw_' + str(id)

    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    if(end == -1):
        sqlText = f"SELECT * FROM sniffer_data WHERE (ts BETWEEN '{start}' AND NOW()) ORDER BY ts ASC"
    else:
        sqlText = f"SELECT * FROM sniffer_data WHERE (ts BETWEEN '{start}' AND '{end}') ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        table = pd.DataFrame(rv,columns=headers)

        nwk = d.v1_nwk(0,0,0)
        nwk.raw_data = table
        nwk.raw_data = nwk.raw_data.applymap(str)
        nwk.v1decode()

        nwk.stats = p.nwk_stats(nwk.parsed_data)
        nwk.stats.get_ave_bw(nwk.parsed_data,winsize)

        return jsonify(len(rv),nwk.stats.ave_bw.to_dict(orient='records'))
    else:
        return jsonify(0,[])

########################
#    Sniffer packet    #
#      count APIs      #
#   AVERAGE BW LITE    #
########################
@app.route('/v1sniffer/<id>/bwlite/recent/<dur>', methods = ['GET'])
@app.route('/v1sniffer/<id>/bwlite/recent/<dur>/winsize/<winsize>', methods = ['GET'])
def get_nwkbwlite(id,dur,winsize='5m'):
    dbname = 'sniffer_data_raw_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)

    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT * from sniffer_data WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        table = pd.DataFrame(rv,columns=headers)

        nwk = d.v1_nwk(0,0,0)
        nwk.raw_data = table
        nwk.raw_data = nwk.raw_data.applymap(str)
        nwk.v1decode()

        nwk.stats = p.nwk_stats(nwk.parsed_data)
        nwk.stats.get_ave_bw(nwk.parsed_data,winsize)

        return jsonify(len(rv),nwk.stats.ave_bw.values.tolist())
    else:
        return jsonify(0,[])

@app.route('/v1sniffer/<id>/bwlite/start/<start>', methods = ['GET'])
@app.route('/v1sniffer/<id>/bwlite/start/<start>/end/<end>', methods = ['GET'])
@app.route('/v1sniffer/<id>/bwlite/start/<start>/winsize/<winsize>', methods = ['GET'])
@app.route('/v1sniffer/<id>/bwlite/start/<start>/end/<end>/winsize/<winsize>', methods = ['GET'])
def get_nwkbwstartendlite(id,start,end=-1,winsize='5m'):
    dbname = 'sniffer_data_raw_' + str(id)

    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    if(end == -1):
        sqlText = f"SELECT * FROM sniffer_data WHERE (ts BETWEEN '{start}' AND NOW()) ORDER BY ts ASC"
    else:
        sqlText = f"SELECT * FROM sniffer_data WHERE (ts BETWEEN '{start}' AND '{end}') ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        table = pd.DataFrame(rv,columns=headers)

        nwk = d.v1_nwk(0,0,0)
        nwk.raw_data = table
        nwk.raw_data = nwk.raw_data.applymap(str)
        nwk.v1decode()

        nwk.stats = p.nwk_stats(nwk.parsed_data)
        nwk.stats.get_ave_bw(nwk.parsed_data,winsize)

        return jsonify(len(rv),nwk.stats.ave_bw.values.tolist())
    else:
        return jsonify(0,[])

############################
# EXPERIMENTAL API FOR NEW #
#   PARSED AGGREGATOR DB   #
############################

@app.route('/aggregator/<id>/sensor2/recent/<dur>', methods = ['GET'])
def get_nodeall_new(id,dur):
    dbname = 'aggre_data_parsed_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT nodeID,nodeLoc,nodeName,s0,s1,s4,s5,ts FROM aggre_packet WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        result = [dict(zip(headers,row)) for row in rv]

        return jsonify(len(rv),result)
    else:
        return jsonify(0,[])

@app.route('/aggregator/<id>/sensor2/start/<start>', methods = ['GET'])
@app.route('/aggregator/<id>/sensor2/start/<start>/end/<end>', methods = ['GET'])
def get_sensor2_start_end(id,start,end=-1):
    dbname = 'aggre_data_parsed_' + str(id)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    #start = start.replace('+',' ')
    if(end == -1):
        sqlText = f"SELECT nodeID,nodeLoc,nodeName,s0,s1,s4,s5,ts FROM aggre_packet WHERE (ts BETWEEN '{start}' AND NOW()) ORDER BY ts ASC"
    else:
        sqlText = f"SELECT nodeID,nodeLoc,nodeName,s0,s1,s4,s5,ts FROM aggre_packet WHERE (ts BETWEEN '{start}' AND '{end}') ORDER BY ts ASC"
        
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    headers = [x[0] for x in cur.description]

    result = [dict(zip(headers,row)) for row in rv]

    return jsonify(len(rv),result)
    
##########################
#  EXPERIMENTAL APIS FOR #
# PERIODIC NETWORK STATS #
##########################
@app.route('/v1sniffer/<id>/pstats/recent/<dur>', methods = ['GET'])
@app.route('/v1sniffer/<id>/pstats/recent/<dur>/winsize/<winsize>', methods = ['GET'])
def get_periodicstats(id,dur,winsize='30m'):
    dbname = 'sniffer_data_stats_' + str(id)
    num = re.split('(\d+)',dur)[1]
    unit = re.split('(\d+)',dur)[2]
    unit = conv_unit(unit)
    
    conn = database.connect(user = user,
                           password = password,
                           host = host,
                           database = dbname)
    
    sqlText = f"SELECT `ts`,`Singlehop`,`Generate`,`Hop`,`Sink`,`MACAck`,`NwkAck`,`Route`,`RouteAck`,`Valid`,`Invalid`,`Retries`,`BW` FROM sniffer_stats_{winsize} WHERE ts >= NOW() - INTERVAL {num} {unit} ORDER BY ts ASC"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    
    if(len(rv)>0):
        headers = [x[0] for x in cur.description]
        result = [dict(zip(headers,row)) for row in rv]

        return jsonify(len(rv),result)
    else:
        return jsonify(0,[])

@app.route('/v1sniffer/<id>/pstats/start/<start>', methods = ['GET'])
@app.route('/v1sniffer/<id>/pstats/start/<start>/end/<end>', methods = ['GET'])
@app.route('/v1sniffer/<id>/pstats/start/<start>/winsize/<winsize>', methods = ['GET'])
@app.route('/v1sniffer/<id>/pstats/start/<start>/end/<end>/winsize/<winsize>', methods = ['GET'])
def get_periodicstats_startend(id,start,end=-1,winsize='30m'):
    dbname = 'sniffer_data_stats_' + str(id)

    conn = database.connect(user = user,
                            password = password,
                            host = host,
                            database = dbname)

    if(end == -1):
        sqlText = f"SELECT `ts`,`Singlehop`,`Generate`,`Hop`,`Sink`,`MACAck`,`NwkAck`,`Route`,`RouteAck`,`Valid`,`Invalid`,`Retries`,`BW` FROM sniffer_stats_{winsize} WHERE (ts BETWEEN '{start}' AND NOW()) ORDER BY ts ASC"
    else:
        sqlText = f"SELECT `ts`,`Singlehop`,`Generate`,`Hop`,`Sink`,`MACAck`,`NwkAck`,`Route`,`RouteAck`,`Valid`,`Invalid`,`Retries`,`BW` FROM sniffer_stats_{winsize} WHERE (ts BETWEEN '{start}' AND '{end}') ORDER BY ts ASC"

    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    headers = [x[0] for x in cur.description]
    result = [dict(zip(headers,row)) for row in rv]

    return jsonify(len(rv),result)

#####################
# APIs FOR AVERAGED #
#    SENSOR DATA    #
#####################
@app.route('/aggregator/stats', methods = ['GET'])
@app.route('/aggregator/stats/winsize/<winsize>', methods = ['GET'])
def get_allstats(winsize='1h'):
    dbname = 'test'

    conn = database.connect(user = user,
                            password = password,
                            host = host,
                            database = dbname)

    sqlText = f"SELECT * FROM stats_{winsize}"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    headers = [x[0] for x in cur.description]
    result = [dict(zip(headers,row)) for row in rv]

    return jsonify(len(rv),result)

#####################
# APIs FOR AVERAGED #
#    SENSOR DATA    #
#   WHOLE NETWORK   #
#####################
@app.route('/aggregator/allstats', methods = ['GET'])
@app.route('/aggregator/allstats/winsize/<winsize>', methods = ['GET'])
def get_allstats(winsize='1h'):
    dbname = 'test'

    conn = database.connect(user = user,
                            password = password,
                            host = host,
                            database = dbname)

    sqlText = f"SELECT * FROM wholenwk_stats_{winsize}"
    cur = conn.cursor()
    cur.execute(sqlText)
    rv = cur.fetchall()
    headers = [x[0] for x in cur.description]
    result = [dict(zip(headers,row)) for row in rv]

    return jsonify(len(rv),result)

#if __name__=="__main__":
#    #app.debug = True
#    app.run(host='0.0.0.0',port=port)
    
    '''from werkzeug.serving import run_simple
    run_simple('localhost', 6969, app)'''
