#!/bin/bash

source bin/activate

gunicorn --workers=2 'rest_main:app' -b 10.158.9.12:2022 --pid pid --log-file demo.log --log-level DEBUG --access-logformat "%(h)s %(s)s %(t)s %(r)s %(U)s %(q)s %(B)s %(M)s" --capture-output --enable-stdio-inheritance --daemon
echo "Server restarted..."
